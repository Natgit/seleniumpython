import time

from selenium.webdriver import Chrome
from selenium.webdriver.common.by import By
from webdriver_manager.chrome import ChromeDriverManager


def test_searching_in_duckduckgo():
    # Uruchomienie przeglądarki Chrome. Ścieżka do chromedrivera
    # ustawiana automatycznie przez bibliotekę webdriver-manager
    browser = Chrome(executable_path=ChromeDriverManager().install())

    # Otwarcie strony duckduckgo
    browser.get('https://duckduckgo.com/')

    # Znalezienie paska wyszukiwania

    search_input = browser.find_element(By.CSS_SELECTOR, "#search_form_input_homepage")

    # Znalezienie guzika wyszukiwania (lupki)

    search_button = browser.find_element(By.CSS_SELECTOR, "#search_button_homepage")

    # Asercje że elementy są widoczne dla użytkownika
    assert search_input.is_displayed() is True
    assert search_button.is_displayed() is True

    # Szukanie Vistula University
    search_input.send_keys("Vistula University")
    search_button.click()

    # Sprawdzenie że pierwszy wynik ma tytuł 'Vistula University in Warsaw'

    # Zamknięcie przeglądarki
    time.sleep(2)
    browser.quit()
