import pytest
from selenium.webdriver import Chrome
from selenium.webdriver.common.by import By
from webdriver_manager.chrome import ChromeDriverManager

from pages.cockpit_page import CockpitPage
from pages.login_page import LoginPage
from pages.projects_page import ProjectsPage


@pytest.fixture()
def browser():
    browser = Chrome(executable_path=ChromeDriverManager().install())
    login_page = LoginPage(browser)
    login_page.load()
    login_page.login("administrator@testarena.pl", "sumXQQ72$L")
    yield browser
    browser.quit()


def test_logout_correctly_displayed(browser):
    assert browser.find_element(By.CSS_SELECTOR, '[title=Wyloguj]').is_displayed() is True


def test_open_administration(browser):
    cockpit_page = CockpitPage(browser)
    cockpit_page.click_administration()

    assert browser.find_element(By.CSS_SELECTOR, '.content_title').text == 'Projekty'


def test_add_new_project(browser):
    projects_page = ProjectsPage(browser)
    projects_page.click_add_project()
    assert browser.find_element(By.CSS_SELECTOR, '.content_title').text == 'Dodaj projekt'

    new_project = projects_page.add_new_project()
    assert browser.find_element(By.CSS_SELECTOR, '#j_info_box > p').text == 'Projekt został dodany.'

    projects_page.click_projects()
    projects_page.search_project(new_project)
    assert browser.find_element(By.LINK_TEXT, new_project).is_displayed()

