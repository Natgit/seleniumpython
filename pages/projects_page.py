from selenium.webdriver.common.by import By
from faker import Faker
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.wait import WebDriverWait


class ProjectsPage:
    def __init__(self, browser):
        self.browser = browser
        self.click_administration()

    def click_administration(self):
        admin_button = self.browser.find_element(By.CSS_SELECTOR, '[title=Administracja]')
        admin_button.click()

    def click_add_project(self):
        add_project = self.browser.find_element(By.CSS_SELECTOR, '.button_link')
        add_project.click()

    def add_new_project(self):
        fake = Faker()
        project_name = fake.country()
        print(f"ADD NEW PROJECT: {project_name}")
        self.browser.find_element(By.CSS_SELECTOR, "#name").send_keys(project_name)
        self.browser.find_element(By.CSS_SELECTOR, "#prefix").send_keys(fake.text().replace(" ", "")[0:6])
        self.browser.find_element(By.CSS_SELECTOR, "#description").send_keys(fake.text())
        self.browser.find_element(By.CSS_SELECTOR, "#save").click()

        wait = WebDriverWait(self.browser, 10)
        selector = (By.CSS_SELECTOR, '.j_close_button')
        wait.until(expected_conditions.element_to_be_clickable(selector))
        return project_name

    def click_projects(self):
        projects = self.browser.find_element(By.LINK_TEXT, 'Projekty')
        projects.click()

    def search_project(self, prj_name):
        self.browser.find_element(By.CSS_SELECTOR, "#search").send_keys(prj_name)
        self.browser.find_element(By.CSS_SELECTOR, "#j_searchButton").click()
